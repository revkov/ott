exports.config = {
    server: {
        ip: '0.0.0.0',
        port: 8811
    },
    redis : {
        host: 'localhost',
        port: 6379,
        expire: 600
    },
    mysql: {
        host: 'localhost',
        port: 3306,
        login: 'revkov',
        password: '',
        database: 'OTT'
    },
    flightstats: {
        url: 'http://www.flightstats.com/go/FlightStatus/flightStatusByAirport.do'
    }
};