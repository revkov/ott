//require modules, config, db clients
require('./include').init();

//init express
var app = express();

//statis files
app.use(express.static(__dirname + '/public'));

//search
app.get('/api/search', function (req, res) {
    api.search(req.query, function (err, result) {
        res.send(result);
    });
});

//results
app.get('/api/results', function (req, res) {
    api.results(req.query, function (err, result) {
        res.send(result); 
    });
});

//start server
var server = app.listen(
    config.server.port,
    config.server.ip,
    function () {
        console.log('Port: %d', server.address().port);
    }
);