var $,
    async,
    tw,
    $TYPE_INPUT,
    $AIR_PORT_INPUT,
    $AIR_LINE_INPUT,
    $SEARCH_BUTTON,
    ENTER_KEY_CODE = 13
;

$(document).ready(function () {

    $TYPE_INPUT = $('#type');
    $AIR_PORT_INPUT = $('#airport');
    $AIR_LINE_INPUT = $('#airline');
    $SEARCH_BUTTON = $('#searchBtn');

    //AUTOCOMPLETE --

    //airport
    var source = [];
    var dataKeys = Object.keys(tw.refData.ru.Airports);

    async.each(
		dataKeys,
        function (v) {
            source.push({id: source.length, name: v});
        },
        function (err) {
            //done
        }
	);

    $AIR_PORT_INPUT.typeahead({
        source: source
    });

    $AIR_LINE_INPUT.typeahead({
        source: []
    });
    
    $TYPE_INPUT.bootstrapToggle({
        on: 'Arrivals',
        off: 'Departures',
        onstyle: 'primary', // [default, primary, success, info, warning, danger]
        offstyle: 'warning' // [default, primary, success, info, warning, danger]
    });
    // --AUTOCOMPLETE

    //EVENTS
    $AIR_PORT_INPUT.bind('keyup', function (e) {
    	if (ENTER_KEY_CODE === e.keyCode) {
    		API.search();
    	}
    });

    $AIR_LINE_INPUT.bind('keyup', function (e) {
    	if (ENTER_KEY_CODE === e.keyCode) {
    		API.search();
    	}
    });

    $SEARCH_BUTTON.bind('click', function (e) {
    	API.search();
    });

});

var API = {
    searchID: null,

    //
    search: function(o) {
        var formData = {
            type: $TYPE_INPUT.prop('checked') ? 1 : 0,
            airport: $AIR_PORT_INPUT.val(),
            airline: $AIR_LINE_INPUT.val()
        };

        $.ajax({
			method: 'GET',
			url: '/api/search',
			data: formData,
			success: function(data){
                if('error'==data.status){
                    //show modal
                }else{
    				API.searchID = data.id;
    				API.results();
                }
			},
		});
    },

    //
    results: function () {
        $.ajax({
			method: 'GET',
			url: '/api/results?id=' + API.searchID,
			success: function (data) {
			    switch (data.status) {
			        case 'done':
			            if (null !== data.results) {
    				        $('#searchRes').html('');
    
    				        if (
    				            0 < data.length
    				            && 'string' === typeof data.results[0].date
    				        ) {
    				            var headID = '#headTableAirline';
    				            var tmplID = '#resRowAirline';
    				        } else {
    				            var headID = '#headTableNotAirline';
    				            var tmplID = '#resRowNotAirline';
    				        }
    
    				        $(headID).tmpl({}).appendTo('#searchRes');
    				        $(tmplID).tmpl(data.results).appendTo('#searchRes');//SHOW RESULTS
    				    }
    				    break;
    				case 'error':
    				    /* error */
    				    break;
    				default:
    				    setTimeout(API.results, 2000);
    				    break;
			    }
			},
		});
    },
};