var init = function () {

    //default modules
    global.express = require('express');
    global.bodyParser = require('body-parser');
    global._ = require('underscore');
    global.cheerio = require('cheerio');
    global.moment = require('moment');
    global.redis = require('redis');
    global.request = require('request');
    global.async = require('async');
    global.JSON2 = require('JSON2');
    global.uuid = require('uuid');
    global.qs = require('qs');

    //my modules
    global.api = require('./functions.js');

    //config
    global.config = require('./config.js').config;

    //connects
    global.redisClient = redis.createClient(config.redis.port, config.redis.host);
    redisClient.on('error', function (err){
        console.log('Error ' + err);
    });
};

exports.init = init;