// method for /api/search
var search = function (data, callBack) {
    var
        searchID = uuid.v4(),
        getData = {airport: data.airport}
    ;

    if (!_.isString(data.airport)) {
        callBack(null, {id: searchID, status: 'error'});
        return;
    }

    callBack(null, {id: searchID, status: 'process'});

    if ('string' == typeof data.airline) {
        getData['airlineToFilter'] = data.airline;
    }

    if ('string' == typeof data.type) {
        getData['airportQueryType'] = data.type;
    }

    var paramDates = (_.isString(data.airline) && 2 == data.airline.length)
        ? 4
        : 12;
    var dates = generateDates(paramDates);

    var parseResult = [];

    async.each(
        dates,
        function (item, next_item) {
            var dataParse = {
                getData: _.extend({}, item, getData),
                timestamp: [
                    moment().add(-1 * paramDates, 'hours').format('X'),
                    moment().add(paramDates, 'hours').format('X')
                ]
            };

            startParser(dataParse, function (err, list) {
                parseResult.push.apply(parseResult, list);
                next_item();
            });
        },
        function () {
            parseResult = _.sortBy(parseResult, function (item) {
                return item.timestamp;
            });

            redisClient.setex(
                searchID,
                config.redis.expire,
                JSON.stringify(parseResult)
            );
        }
    );
};

//
var generateDates = function (param) {
    var results = [];
    if(4 == param){// +/- 4
        var dateMinus4 = moment().add(-4, 'hours');
        var datePlus4 = moment().add(4, 'hours');
        results.push({
            airportQueryDate: dateMinus4.format('YYYY-MM-DD'),
            airportQueryTime: getTimeParam( dateMinus4.format('HH') ),
        });
        results.push({
            airportQueryDate: datePlus4.format('YYYY-MM-DD'), 
            airportQueryTime: getTimeParam( datePlus4.format('HH') ),
        });
    }else if(12 == param){// +/- 12
        var dateMinus12 = moment().add(-12, 'hours');
        var dateMinus6 = moment().add(-6, 'hours');
        var dateCurrent = moment();
        var datePlus6 = moment().add(6, 'hours');
        var datePlus12 = moment().add(12, 'hours');
        results.push({
            airportQueryDate: dateMinus12.format('YYYY-MM-DD'),
            airportQueryTime: getTimeParam( dateMinus12.format('HH') ),
        });
        results.push({
            airportQueryDate: dateMinus6.format('YYYY-MM-DD'),
            airportQueryTime: getTimeParam( dateMinus6.format('HH') ),
        });
        results.push({
            airportQueryDate: dateCurrent.format('YYYY-MM-DD'),
            airportQueryTime: getTimeParam( dateCurrent.format('HH') ),
        });
        results.push({
            airportQueryDate: datePlus6.format('YYYY-MM-DD'),
            airportQueryTime: getTimeParam( datePlus6.format('HH') ),
        });
        results.push({
            airportQueryDate: datePlus12.format('YYYY-MM-DD'),
            airportQueryTime: getTimeParam( datePlus12.format('HH') ),
        });
    }

    return results;
};

//
var getTimeParam = function (time) {

    if (time >= 0 && time < 6) {
        time = 0;
    } else if (time >= 6 && time < 12) {
        time = 6;
    } else if (time >= 12 && time < 18) {
        time = 12;
    } else if (time >= 18) {
        time = 18;
    }

    return time;
}

//parser
var startParser = function (data, callBack) {
    var uri = config.flightstats.url + '?' + qs.stringify(data.getData);

    request({uri:uri}, function (error, response, body) {

        if (error || response.statusCode != 200) {
            callBack('startParser: ERROR');
            return;
        }

        var
            $ = cheerio.load(body),
            list = [],
            trArr = $('.tableListingTable tr[class!=tableHeader]')
        ;

        trArr.each(function (i, tr) {
            var
                r = {},
                tdArr = $('td', tr)
            ;

            tdArr.each(function (i, td) {
                if(0==i) r.destination = $(td).text().replace(/\n/g,'');
                else if(1==i) r.flight = $('a',td).text();
                else if(3==i) r.airline = $(td).text();
                else if(4==i) r.sched = $(td).text().replace(/\n/g,'');
                else if(5==i) r.actual = $(td).text().replace(/\n|\~/g,'');
                else if(6==i) r.gate = $(td).html().replace(/\n/g,'').replace(/\<br\>/g,' ');
                else if(7==i) r.status = $(td).text().replace(/\n/g,'');
                else if(8==i) r.equip = $(td).text().replace(/\n/g,'');
            });

            r.daterow = moment(data.getData.airportQueryDate + ' ' + r.actual, 'YYYY-MM-DD hh:mm A')
                .format('YYYY-MM-DD');
            r.timestamp = moment(data.getData.airportQueryDate + ' ' + r.actual, 'YYYY-MM-DD hh:mm A')
                .format('X');

            if (r.timestamp >= data.timestamp[0] && r.timestamp <= data.timestamp[1]) {
                list.push(r);
            }
        });

        callBack(null, list);
    });
};

// method /api/results
var results = function (data, callBack) {

    if (!data.id) {
        return callBack(null, {status: 'error'});
    }

    redisClient.get(data.id, function (err, results) {
        if (err) {
            return callBack(null, {status:'process'});
        }

        callBack(
            null,
            {
                results: JSON.parse(results),
                status: (null == results ? 'process' : 'done')
            }
        );
    });
};

exports.search = search;
exports.results = results;